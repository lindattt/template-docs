Basics of technical documentation: a project report for the "Abilità informatiche" class
------------------------------------------------------------------------------------------------------

This is the project report related to the "Abilità informatiche" class.

The goals of the project were: 

1.	to explore some of the tools available to generate technical documentation;
2.	to use those tools to create some technical documentation about a chosen topic, in the form of a website;
3.  to create a report describing the job done.


In order to fulfill the second goal, the natural choice was to create technical documentation about *the process of creating technical documentation*. This means that the webpage you are looking at right now is both *the project* (second goal) and *the project report* (third goal) itself.

In other words, this project can be considered as a tutorial that will guide you through the process of writing documentation for a given product (a software, a code, ...) and hosting it on the web.

To have an overview of the steps covered in this tutorial, click here. (INSERIRE LINK DELLA PAGINA OVERVIEW)


..
   :hidden: permette di nascondere il toctree

.. toctree::
   :hidden: 
   
   overview
   git
   sphinx
   markup
   host
   orgmode
   temi





