Overview
--------

Welcome to the tutorial on how to create technical documentation.

In this tutorial you will learn how to ...

To start, you'll just need to have a text editor and basic knowledge of shell.

In order to generate your own documentation, those are the tools that you will need:

*   a version control platform, like **GitHub** or **GitLab**;
*   a documentation compiler, like **Sphinx**;
*   a markup language, like **Markdown** or **reStructured Text**;
*   a hosting service, like **read the docs** (or the git platforms theirselves).

To make your experience more fluent and personal, you may also like to have a look on:

* **Org-mode**, a major mode for *GNU Emacs* text editor, a markup language and much more;
* how to change and customize **themes** in your webpage.



