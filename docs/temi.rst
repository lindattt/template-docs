=======
Themes
=======

Sphinx supports changing the appearance of its HTML output via themes. A theme is a collection of HTML templates, stylesheet(s) and other static files. Additionally, it has a configuration file which specifies from which theme to inherit, which highlighting style to use, and what options exist for customizing the theme’s look and feel.

Themes are meant to be project-unaware, so they can be used for different projects without change.

There are plenty of free themes that you can choose. Here you can find some examples: https://sphinx-themes.org


How to change the theme of your project?
It is simple. You need to install the theme 

.. code-block:: shell

	pip install



and then change the document ``conf.py``

.. code-block:: shell

	theme
